
/* Evento que busca el div "init" para desplegar la pagina
 * inicializacion de eventos corresponfientes a los botones qr_btn y map_btn
 */
$(document).on("pageinit", "#init", function(e) {
    //Funcion para leer el codigo QR cuando se le da un click al boton "qr_btn"
    $("#qr_btn").on("click", function() {
        cordova.plugins.barcodeScanner.scan(function(result) {
            //Pone en el tag small el texto de las coordenadas leidas.
            $("#result_p").html(result.text);
            //Llama la funcion que muestra el mapa y el marcador en el punto
            //leido
            mapFromQR();
        }, function(error) {
            alert("Scan failed: " + error);
        });

    });
    //Funcion para ir al ultimo punto leido por el qr
    $("#map_btn").on("click", function() {
        //Llama la funcion que muestra el mapa y el marcador en el punto
        //leido
        mapFromQR();  
    });        
});
//Funcion para mostrar el mapa
function mapFromQR(){
    //Muestra el div donde se encuentra el mapa.
    $("#map").removeClass("hidden").addClass("show");
    //Se asigna el div map_canvas para mostrar el mapa allí
    var map_canvas = document.getElementById("map_canvas");
    //Se crea el mapa y se muestra en el div correspondiente, con la ayuda del
    //plugin de cordova.
    var map = plugin.google.maps.Map.getMap(map_canvas);
    //Se toman las cordenadas que se leyeron.
    var coordinate = $("#result_p").html().split(",");
    //Se asignan las coordenadas para crear un punto en el mapa.
    var coordinates = new plugin.google.maps.LatLng(
                        parseFloat(coordinate[0]), 
                        parseFloat(coordinate[1]));
    alert(coordinate[0]);
    alert(coordinate[1]);
    //Se crea el marcador con las opciones necesarias.
    map.addMarker({
        'position': coordinates,
        'draggable': true,
        'title': "Miau!!!",
        'snippet': "Aqui hay un lindo gatito."
    }, function(marker) {
        //Se muestra la informacion del marcador.
        marker.showInfoWindow();
    });
    //Crea una animación que centra el mapa en el marcador.
    map.animateCamera({
        'target': coordinates,
        'zoom': 15,
        'tilt': 45,
        'bearing': 90
    });
    
}